<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use DB;
class crud extends Controller
{
    public function list(Request $req){
        // $req['page']
        $s = $req['search'];
        if ($s == ''||$s == null) {
            $value = User::paginate(5);
        }else{
            $value = User::where('name','like','%'.$s.'%')->paginate(5);
        }
    	return response()->json([
    		'text'=>'successfully retreaved',
    		'data'=>$value
    	]);
    }

    public function view(Request $req){
            $data = User::where('id',array_column($req->all(),'id'))->get();
            return response()->json([
            
            'message' => "Search Successfully",
            'data'=> $data

            ]);
    }

    public function delete(Request $req){
    		$e = array_column($req->all(),'id');
    		$email = array_column($req->all(),'email');
    		$id = implode($e);
    		User::destroy($id);
    	return response()->json([
    		'message' => "Delete Successfully",
    		'data'=> $email

    	]);
    }





    public function edit(Request $req){
    	$val = Validator::make($req->all(),[
    		'name' => 'required|max:255',
            'email' => 'required|email:filter|max:255',
        ]);
        if ($val->fails()) {
    		return response()->json([
    			'errors'    =>  $val->errors()
    		],400);
    	}else{
		User::where('id', $req['id'])
                ->update([
                	'name' => $req['name'],
                	'email' => $req['email']
                 ]);
		return response()->json([
    		'message' => "Edit Successfully",
    		'data'=>$req['email']
    	]);
    	}
    	
    }





    public function add(Request $req){
    	$val = Validator::make($req->all(),[
    		'name' => 'required|max:255',
            'email' => 'required|email:filter|max:255|unique:users',
            'pass' => 'required',
    	]);
    	if ($val->fails()) {
    		return response()->json([
    			'errors'    =>  $val->errors()
    		],400);
    	}else{
    		DB::beginTransaction();
    		try{
    			USER::create([
    				'name' => $req['name'],
		            'email' => $req['email'],
		            'password' => bcrypt($req['pass']),
    			]);
    			DB::commit();
    			return response()->json([
    				'message' => "Data Saved"
    			]);
    		}catch(\Exception $ex){
	            DB::rollback();
	            return response()->json([
	                'errors'    =>  [ 'error crudAdd' ],
	                'message'   =>  $ex->getMessage()
	            ],500);
	        }
    	}
    }
}
