<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class comments extends Model
{
    use HasFactory;


    protected $fillable = [
        'comment',
        'user_id',
        'blog_id'
    ];
    protected $with = [
        'blog'
    ];
    public function blog()
    {
        return $this->belongsTo('App\Models\blog');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
