<?php

namespace Database\Seeders;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       User::create([
            'name'          =>  'Sean Russell D. Vargas',
            'email'         =>  'laurenze97@gmail.com',
            'password'      =>  bcrypt('developer'),
        ]);
    }
}
