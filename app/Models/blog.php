<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class blog extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'user_id',
        'content'
    ];
    protected $with = [
        'user'
    ];
    public function comments()
    {
        return $this->hasMany('App\Models\comments');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }
}
