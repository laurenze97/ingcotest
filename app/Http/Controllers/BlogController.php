<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\blog;
use App\Models\User;
use Validator;
use DB;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        $val = Validator::make($request->all(),[
            'title' => 'required|max:255',
            'content' => 'required|max:255',
        ]);
        if ($val->fails()) {
            return response()->json([
                'errors'    =>  $val->errors()
            ],400);
        }else{
            $data = User::where('email',$request->email)->get();
            DB::beginTransaction();
            try{
                blog::create([
                    'user_id' => $data[0]['id'],
                    'title' => $request['title'],
                    'content' => $request['content']
                ]);
                DB::commit();
                return response()->json([
                    'message' => "Data Saved!"
                ]);
            }catch(\Exception $ex){
                DB::rollback();
                return response()->json([
                    'errors'    =>  [ 'error blogadd' ],
                    'message'   =>  $ex->getMessage()
                ],500);
            }

            // return response()->json([
            // 'data'=> $data[0]['id']
            // ]);
            // return response()->json([
            //     'payload'    =>  $request->all()
            // ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $userdata = blog::where('user_id',$request->user_id)->orderBy('created_at','DESC')->get();
        return response()->json([
            'payload'    =>  $userdata
        ]);    


        return response()->json([
            'data'    =>  $request->user_id
        ]);
        

    }
    public function showall()
    {
        $userdata = blog::orderBy('created_at','DESC')->get();
        return response()->json([
            'payload'    =>  $userdata
        ]);    


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(blog $blog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, blog $blog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(blog $blog)
    {
        //
    }
}
